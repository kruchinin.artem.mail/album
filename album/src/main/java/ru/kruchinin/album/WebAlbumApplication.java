package ru.kruchinin.album;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebAlbumApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebAlbumApplication.class, args);
    }

}
