package ru.kruchinin.album.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.kruchinin.album.sevices.UserDetailsServices;

/**
 * @author Kruchinin Artem
 */
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final UserDetailsServices userDetailsServices;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/admin").hasRole("ADMIN")
                .antMatchers("/moderator").hasAnyRole("ADMIN", "MODERATOR")
                .antMatchers("/registration", "/error", "/rules", "/static/**", "/activate/*", "/img/**", "/remember-password/**", "/find-image").permitAll()
                .anyRequest().hasAnyRole("USER", "MODERATOR", "ADMIN")
                .and()
                .formLogin().loginPage("/").permitAll()
                .loginProcessingUrl("/process_login")
                .defaultSuccessUrl("/profile", true)
                .failureUrl("/?error")
                .and()
                .rememberMe()
                .and()
                .logout()
                .logoutSuccessUrl("/")
                .permitAll();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsServices)
                .passwordEncoder(getPasswordEncoder());
    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
