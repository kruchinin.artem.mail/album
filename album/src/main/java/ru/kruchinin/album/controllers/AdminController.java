package ru.kruchinin.album.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.kruchinin.album.models.Roles;
import ru.kruchinin.album.models.User;
import ru.kruchinin.album.sevices.UserServices;

/**
 * @author Kruchinin Artem
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("/admin")
public class AdminController {
    private final UserServices userServices;

    @GetMapping
    public String adminPage(Model model) {
        model.addAttribute("user", userServices.getAllUsers());
        model.addAttribute("roles", Roles.values());
        return "other-page/admin";
    }

    @PostMapping("/edit-role/{id}")
    public String editRole(@RequestParam("roleName") Roles roleFromForm, @PathVariable int id){
        userServices.changeRole(id, roleFromForm);
        return "redirect:/admin";
    }

    @PostMapping("/ban/{id}")
    public String banUser(@PathVariable int id){
        userServices.banUser(id);
        return "redirect:/admin";
    }

    @PostMapping("/new-moderator")
    public String addModerator(User user) {
        return "redirect:/admin";
    }
}
