package ru.kruchinin.album.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.kruchinin.album.models.Album;
import ru.kruchinin.album.sevices.AlbumServices;
import ru.kruchinin.album.sevices.ImageServices;
import ru.kruchinin.album.sevices.UserServices;

import javax.validation.Valid;


@Controller
@RequiredArgsConstructor
public class AlbumController {
    private final AlbumServices albumServices;
    private final ImageServices imageServices;
    private final UserServices userServices;


    @PostMapping("album/add")
    public String addAlbum(@ModelAttribute("album") @Valid Album album, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", bindingResult.getFieldError());
            model.addAttribute("album", albumServices.listAlbums(userServices.authUser().getId()));
            model.addAttribute("user", userServices.authUser());
            return "profile-page";
        }
        album.setUser(userServices.authUser());
        albumServices.saveAlbum(album);
        return "redirect:/profile";
    }

    @GetMapping("/album/{id_album}")
    public String albumInfo(@PathVariable int id_album, Model model) {
        model.addAttribute("album", albumServices.getAlbumById(id_album));
        model.addAttribute("image", imageServices.listImages(id_album));
        model.addAttribute("user", userServices.authUser());
        return "album-page";
    }

    @PostMapping("album/delete/{id_album}")
    public String deleteAlbum(@PathVariable int id_album) {
        albumServices.deleteAlbum(id_album);
        return "redirect:/profile";
    }

    @PostMapping("/album/update/{id_album}")
    public String editeAlbum(@ModelAttribute("album") Album updatedAlbum, @PathVariable int id_album) {
        albumServices.updateAlbum(updatedAlbum, id_album);
        return "redirect:/album/{id_album}";
    }
}
