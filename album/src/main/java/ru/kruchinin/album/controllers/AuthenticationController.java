package ru.kruchinin.album.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.kruchinin.album.models.User;
import ru.kruchinin.album.sevices.AuthenticationServices;
import ru.kruchinin.album.sevices.ImageServices;
import ru.kruchinin.album.sevices.UserServices;

import javax.validation.Valid;

/**
 * @author Kruchinin Artem
 */
@Controller
@RequiredArgsConstructor
public class AuthenticationController {
    private final AuthenticationServices authenticationServices;
    private final ImageServices imageServices;
    private final UserServices userServices;


    @GetMapping("/")
    public String loginPage(Model model) {
        model.addAttribute("image", imageServices.listAllNotPrivateImages());
        model.addAttribute("allUsers", userServices.getAllUsers().size());
        return "home-page";
    }


    @PostMapping("/registration")
    public String addUser(@ModelAttribute("newUser") @Valid User user, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", bindingResult.getFieldError());
            return "home-page";
        }

        if (authenticationServices.checkUserByUserName(user.getUserName())) {
            bindingResult.rejectValue("userName", "", "Имя пользователя уже существует");
            return "home-page";
        }
        if (authenticationServices.checkUserByUserEmail(user.getUserEmail())) {
            bindingResult.rejectValue("userEmail", "", "Email пользователя уже существует");
            return "home-page";
        }

        authenticationServices.registerNewUser(user);
        return "redirect:/";
    }

    @GetMapping("/activate/{code}")
    public String activationNewUser(@PathVariable String code) {
        authenticationServices.activate(code);
        return "redirect:/home";
    }

    @GetMapping("/remember-password")
    public String rememberPassword() {
        return "other-page/remember-password";
    }

    @PostMapping("/remember-password")
    public String rememberPassword(@RequestParam("userEmail") String userEmail) {
        if (authenticationServices.checkUserByUserEmail(userEmail)) {
            authenticationServices.rememberPassword(userEmail);
        }
        return "redirect:/";
    }

    @GetMapping("/remember-password/change/{code}")
    public String rememberPasswordAfterEmail(@PathVariable String code) {
        if (authenticationServices.checkActivationCode(code)) {
            return "other-page/change-password";
        }
        return "redirect:/";
    }

    @PostMapping("/remember-password/change/{code}")
    public String rememberPasswordAfterEmail(@RequestParam("userPassword") String password, @PathVariable String code) {
        if (password.length() >= 6) {
            authenticationServices.changePassword(password, code);
            return "redirect:/";
        }
        return "redirect:/remember-password/change/{code}";
    }

}
