package ru.kruchinin.album.controllers;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kruchinin.album.sevices.CommentServices;

/**
 * @author Kruchinin Artem
 */
@Controller
@AllArgsConstructor
public class CommentController {
    private final CommentServices commentServices;

    @PostMapping("/{id_image}/add-comment")
    public String addComment(@RequestParam("newComment") String newComment, @PathVariable int id_image, Model model) {
        if (newComment.length() >= 2000) {
            return "redirect:/image/{id_image}";
        }
        commentServices.addNewComment(newComment, id_image);
        return "redirect:/image/{id_image}";
    }


    @PostMapping("/image/{id_image}/comment/{id_comment}/edit")
    public String editComment(@RequestParam String comment, @PathVariable int id_image, @PathVariable int id_comment) {
        commentServices.editeComment(comment, id_comment);
        return "redirect:/image/{id_image}";
    }


    @PostMapping("/image/{id_image}/comment/{id_comment}/delete")
    public String deleteComment(@PathVariable int id_image, @PathVariable int id_comment) {
        commentServices.deleteComment(id_comment);
        return "redirect:/image/{id_image}";
    }

}
