package ru.kruchinin.album.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.kruchinin.album.models.Image;
import ru.kruchinin.album.sevices.CommentServices;
import ru.kruchinin.album.sevices.ImageServices;
import ru.kruchinin.album.sevices.UserServices;

import java.io.IOException;

/**
 * @author Kruchinin Artem
 */
@Controller
@RequiredArgsConstructor
public class ImageController {
    private final ImageServices imageServices;
    private final CommentServices commentServices;
    private final UserServices userServices;


    @PostMapping("/album/{id_album}/image/add")
    public String addImage(@RequestParam("name") String name, @PathVariable int id_album, @RequestParam("file") MultipartFile file, Image image) throws IOException {
        image.setName(name);
        imageServices.addImage(image, id_album, file);
        return "redirect:/album/{id_album}";
    }

    @GetMapping("/image/{id_image}")
    public String infoImage(@RequestParam(value = "edit", required = false) boolean edit, @PathVariable int id_image, Model model) {
        model.addAttribute("image", imageServices.getImageById(id_image));
        model.addAttribute("comments", commentServices.listComments(id_image));
        model.addAttribute("user", userServices.authUser());
        if (edit)
            model.addAttribute("edit", edit);
        return "image-page";
    }

    @GetMapping("/find-image")
    public String findImage(@RequestParam(required = false, defaultValue = "") String filter, Model model) {

        if (!filter.isEmpty() && filter != null) {
            model.addAttribute("findImage", imageServices.getFindImage(filter));
        }

        model.addAttribute("image", imageServices.listAllNotPrivateImages());
        model.addAttribute("allUsers", userServices.getAllUsers().size());
        return "home-page";
    }

    @PostMapping("/image/delete/{id_image}")
    public String deleteImage(@PathVariable int id_image) {
        imageServices.deleteImage(id_image);
        return "redirect:/";
    }

    @PostMapping("/image/{id_image}/edite")
    public String updateImage(@ModelAttribute("image") Image image, @PathVariable int id_image) {
        imageServices.updateImage(image, id_image);
        return "redirect:/image/{id_image}";
    }

    @PostMapping("/{id_image}/gray")
    public String imageToGray(@PathVariable int id_image) {
        imageServices.imageToGray(id_image);
        return "redirect:/image/{id_image}";
    }

    @PostMapping("{id_image}/rotate")
    public String imageRotate(@RequestParam(value = "rad", defaultValue = "0") int rad, @PathVariable int id_image) {
        imageServices.imageRotateR(id_image, rad);
        return "redirect:/image/{id_image}";
    }

    @PostMapping("/{id_image}/resize")
    public String imageResize(@RequestParam(value = "size", defaultValue = "0") int size, @PathVariable int id_image) {
        imageServices.imageResize(id_image, size);
        return "redirect:/image/{id_image}";
    }

}
