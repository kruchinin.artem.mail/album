package ru.kruchinin.album.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kruchinin.album.sevices.UserServices;

/**
 * @author Kruchinin Artem
 */
@Controller
@RequiredArgsConstructor
public class ModeratorController {
    private final UserServices userServices;

    @GetMapping("/moderator")
    public String moderator(@RequestParam(required = false, defaultValue = "") String filter, Model model) {

        if (!filter.isEmpty() && filter != null) {
            model.addAttribute("findUser", userServices.getUserByName(filter));
        }

        model.addAttribute("user", userServices.getAllUsers());
        return "other-page/moderator";
    }

}
