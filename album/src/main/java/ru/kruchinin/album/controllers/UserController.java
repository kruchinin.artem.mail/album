package ru.kruchinin.album.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.kruchinin.album.models.User;
import ru.kruchinin.album.sevices.AlbumServices;
import ru.kruchinin.album.sevices.AuthenticationServices;
import ru.kruchinin.album.sevices.UserServices;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * @author Kruchinin Artem
 */
@Controller
@RequiredArgsConstructor
public class UserController {
    private final UserServices userServices;
    private final AuthenticationServices authenticationServices;
    private final AlbumServices albumServices;

    @GetMapping("/profile")
    public String profilePage(Model model) {
        model.addAttribute("album", albumServices.listAlbums(userServices.authUser().getId()));
        model.addAttribute("user", userServices.authUser());
        return "profile-page";
    }


    @PostMapping("/profile/update")
    public String updateUserEmail(@ModelAttribute("user") @Valid User user, BindingResult bindingResult) {
        System.out.println(user.getUserEmail());
        if (authenticationServices.checkUserByUserEmail(user.getUserEmail())) {
            bindingResult.rejectValue("userEmail", "", "Email пользователя уже существует");
            return "redirect:/profile";
        }
        userServices.updateUserEmailWaitActivation(user.getUserEmail(), userServices.authUser().getId());
        return "redirect:/profile";
    }

    @GetMapping("user/update-email/{code}")
    public String updateUserEmail(@PathVariable String code) {
        userServices.updateUserEmail(code);
        return "redirect:/";
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            request.getSession().invalidate();
        }
        return "redirect:/";
    }

    @PostMapping("/profile/delete")
    public String deleteUser() {
        userServices.delete(userServices.authUser().getId());
        return "redirect:/";
    }

}
