package ru.kruchinin.album.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

/**
 * @author Kruchinin Artem
 */
@Entity
@Data
@Table(name = "comment")
@AllArgsConstructor
@NoArgsConstructor
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "comment")
    @Size(max = 2000)
    private String comment;

    @Column(name = "date_of_created")
    private LocalDateTime dateOfCreated;

    @ManyToOne()
    @JoinColumn(name = "image_id", referencedColumnName = "id")
    private Image image;

    @OneToOne()
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @PrePersist
    private void initDate() {
        dateOfCreated = LocalDateTime.now();
    }
}
