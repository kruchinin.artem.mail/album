package ru.kruchinin.album.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "image")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Image {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "only_for_me")
    private boolean onlyForMe;

    @ManyToOne()
    @JoinColumn(name = "album_id", referencedColumnName = "id")
    private Album album;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "image")
    private List<Comment> comment;

}
