package ru.kruchinin.album.models;

/**
 * @author Kruchinin Artem
 */
public enum Roles {
    WAIT_ACTIVATION, ROLE_USER, ROLE_ADMIN, ROLE_MODERATOR;
}
