package ru.kruchinin.album.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "user_table")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "user_name")
    @NotEmpty(message = "Can't be empty")
    @Size(min = 6, max = 20, message = "Min 6, max 20")
    private String userName;

    @Column(name = "user_password")
    @NotEmpty(message = "Can't be empty")
    @Size(min = 6, message = "Min 6")
    private String userPassword;

    @Column(name = "user_email")
    @NotEmpty(message = "Can't be empty")
    @Email
    private String userEmail;

    @Column(name = "active")
    private boolean active;

    @Column(name = "check_Rules")
    @AssertTrue(message = "You have not accepted the user agreement")
    private boolean checkRules;

    @Column(name = "date_of_created")
    private LocalDateTime dateOfCreated;

    @Column(name = "user_role")
    private String role;

    @Column(name = "activation_code")
    private String activationCode;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<Album> albums = new ArrayList<>();

    public boolean isAdmin() {
        return role.contains(Roles.ROLE_ADMIN.name());
    }

    @PrePersist
    private void initDate() {
        dateOfCreated = LocalDateTime.now();
    }
}
