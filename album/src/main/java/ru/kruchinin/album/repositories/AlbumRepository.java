package ru.kruchinin.album.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.kruchinin.album.models.Album;

import java.util.List;

@Repository
public interface AlbumRepository extends JpaRepository<Album, Integer> {
    List<Album> findAllByUserId(int user_id);

    @Query(value = "select max(id) from Album")
    Integer findMaxById();
}
