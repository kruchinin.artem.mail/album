package ru.kruchinin.album.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kruchinin.album.models.Comment;

import java.util.List;

/**
 * @author Kruchinin Artem
 */
@Repository
public interface CommentsRepository extends JpaRepository<Comment, Integer> {
    List<Comment> findAllByImageId(int id);

}
