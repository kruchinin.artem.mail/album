package ru.kruchinin.album.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.kruchinin.album.models.Image;

import java.util.List;

@Repository
public interface ImageRepository extends JpaRepository<Image, Integer> {

    List<Image> findByAlbumId(int albumId);

    List<Image> findAllByOnlyForMeFalse();

    List<Image> findAllByName(String name);

    @Query(value = "SELECT MAX(id) from Image")
    Integer findMaxById();

}

