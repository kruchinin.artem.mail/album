package ru.kruchinin.album.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kruchinin.album.models.User;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    Optional<User> findByUserName(String name);

    Optional<User> findByUserEmail(String email);

    Optional<User> findByActivationCode(String code);
}
