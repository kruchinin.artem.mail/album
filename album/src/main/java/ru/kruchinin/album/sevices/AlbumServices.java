package ru.kruchinin.album.sevices;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.kruchinin.album.models.Album;
import ru.kruchinin.album.repositories.AlbumRepository;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class AlbumServices {
    private final AlbumRepository albumRepository;

    public List<Album> listAlbums(int user_id) {
        return albumRepository.findAllByUserId(user_id);
    }

    public void saveAlbum(Album album) {
        if (album.getName().equals("")) {
            album.setName("Album #" + (albumRepository.findMaxById() + 1));
        }
        log.info("Saving new Album. Name: {};", album.getName());
        albumRepository.save(album);
    }

    public void deleteAlbum(int id) {
        albumRepository.deleteById(id);
    }

    public Album getAlbumById(int id) {
        return albumRepository.findById(id).orElse(null);
    }

    public void updateAlbum(Album albumUpdate, int id_album) {
        Album album = albumRepository.findById(id_album).orElse(null);
        album.setName(albumUpdate.getName());
        albumRepository.save(album);
    }
}
