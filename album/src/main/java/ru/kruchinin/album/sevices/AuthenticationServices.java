package ru.kruchinin.album.sevices;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.kruchinin.album.models.Roles;
import ru.kruchinin.album.models.User;
import ru.kruchinin.album.repositories.UserRepository;

import java.util.UUID;


@Service
@RequiredArgsConstructor
public class AuthenticationServices {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JavaMailSender mailSender;
    @Value("${spring.mail.username}")
    private String webAppMail;


    public void registerNewUser(User user) {
        user.setUserPassword(passwordEncoder.encode(user.getUserPassword()));
        user.setRole(Roles.WAIT_ACTIVATION.name());
        user.setActivationCode(UUID.randomUUID().toString());
        userRepository.save(user);
        String message = String.format("Hello, %s!\nWe are glad to welcome you on our site \"WebAlbum\"!\nTo activate your account follow the link\nhttp://localhost:8080/activate/%s", user.getUserName(), user.getActivationCode());
        sendEmail(user.getUserEmail(), "Activation code", message);
    }

    public boolean checkUserByUserName(String userName) {
        return userRepository.findByUserName(userName).isPresent();
    }

    public boolean checkUserByUserEmail(String userEmail) {
        return userRepository.findByUserEmail(userEmail).isPresent();
    }

    public void activate(String code) {
        User user = userRepository.findByActivationCode(code).orElse(null);
        if (user == null) {
            return;
        }
        user.setRole(Roles.ROLE_USER.name());
        user.setActive(true);
        user.setActivationCode(null);
        userRepository.save(user);
    }

    public void rememberPassword(String userEmail) {
        User user = userRepository.findByUserEmail(userEmail).orElse(null);
        user.setActivationCode(UUID.randomUUID().toString());
        userRepository.save(user);
        String message = String.format("Hello, %s!\nYou have sent a request to change your password.\nPlease follow the link for further steps:\nhttp://localhost:8080/remember-password/change/%s", user.getUserName(), user.getActivationCode());
        sendEmail(user.getUserEmail(), "Change password", message);
    }

    public void changePassword(String password, String code) {
        User user = userRepository.findByActivationCode(code).orElse(null);
        user.setUserPassword(passwordEncoder.encode(password));
        user.setActivationCode(null);
        userRepository.save(user);
    }

    public boolean checkActivationCode(String code) {
        return userRepository.findByActivationCode(code).isPresent();
    }

    public void sendEmail(String toEmail, String subject, String bodyEmail) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom(webAppMail);
        mailMessage.setTo(toEmail);
        mailMessage.setText(bodyEmail);
        mailMessage.setSubject(subject);
        mailSender.send(mailMessage);
    }

}
