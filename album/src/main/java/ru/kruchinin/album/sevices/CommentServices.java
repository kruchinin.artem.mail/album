package ru.kruchinin.album.sevices;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.kruchinin.album.models.Comment;
import ru.kruchinin.album.repositories.CommentsRepository;
import ru.kruchinin.album.repositories.ImageRepository;

import java.util.List;

/**
 * @author Kruchinin Artem
 */
@Service
@AllArgsConstructor
public class CommentServices {
    private final CommentsRepository commentsRepository;
    private final ImageRepository imageRepository;
    private final UserServices userServices;


    public void addNewComment(String newComment, int id_image) {
        Comment comment = new Comment();
        comment.setComment(newComment);
        comment.setImage(imageRepository.findById(id_image).orElse(null));
        comment.setUser(userServices.authUser());
        commentsRepository.save(comment);
    }

    public List<Comment> listComments(int id_image) {
        return commentsRepository.findAllByImageId(id_image);
    }

    public void deleteComment(int id_comment) {
        commentsRepository.deleteById(id_comment);
    }

    public void editeComment(String comment, int id_comment) {
        Comment updatedComment = commentsRepository.findById(id_comment).orElse(null);
        updatedComment.setComment(comment);
        commentsRepository.save(updatedComment);
    }

}
