package ru.kruchinin.album.sevices;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.kruchinin.album.models.Image;
import ru.kruchinin.album.repositories.ImageRepository;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.UUID;


@Service
@Slf4j
@RequiredArgsConstructor
public class ImageServices {
    private final ImageRepository imageRepository;
    private final AlbumServices albumServices;
    @Value("${upload.path}")
    private String uploadPath;

    public List<Image> listImages(int albumId) {
        return imageRepository.findByAlbumId(albumId);
    }

    public List<Image> listAllNotPrivateImages() {
        List<Image> list = imageRepository.findAllByOnlyForMeFalse();
        Collections.shuffle(list);
        return list;
    }

    public void addImage(Image image, int id_album, MultipartFile file) throws IOException {
        if (image.getName().equals("")) {
            image.setName("Image #" + (imageRepository.findMaxById() + 1));
        }
        if (!file.isEmpty()) {
            File uploadDir = new File(uploadPath);
            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }
            String uuidFile = UUID.randomUUID().toString();
            String resultFileName = uuidFile + "." + file.getOriginalFilename();
            file.transferTo(new File(uploadPath + "/" + resultFileName));
            image.setFileName(resultFileName);
            log.info("Save new image: {}", image.getName());
            image.setAlbum(albumServices.getAlbumById(id_album));
            imageRepository.save(image);
        }
    }


    public void deleteImage(int id) {
        Image image = imageRepository.findById(id).orElse(null);
        if (image != null) {
            File file = new File(uploadPath + "/" + image.getFileName());
            file.delete();
        }
        imageRepository.deleteById(id);
        log.info("Image deleted");
    }

    public Image getImageById(int id) {
        return imageRepository.findById(id).orElse(null);
    }

    public List<Image> getFindImage(String name) {

        return imageRepository.findAllByName(name);
    }

    public void updateImage(Image updatedImage, int id_image) {
        Image image = imageRepository.findById(id_image).orElse(null);
        if (image != null) {
            image.setName(updatedImage.getName());
            imageRepository.save(image);
        }
    }

    public void imageToGray(int id) {
        Image oldImage = imageRepository.findById(id).orElse(null);
        String oldFileName = oldImage.getFileName();
        try {
            File file = new File(uploadPath + "/" + oldImage.getFileName());
            BufferedImage sourceImage = ImageIO.read(file);
            BufferedImage newImage = new BufferedImage(sourceImage.getWidth(), sourceImage.getHeight(), sourceImage.getType());
            for (int x = 0; x < sourceImage.getWidth(); x++) {
                for (int y = 0; y < sourceImage.getHeight(); y++) {
                    Color color = new Color(sourceImage.getRGB(x, y));
                    int blue = color.getBlue();
                    int red = color.getRed();
                    int green = color.getGreen();
                    int gray = (int) (red * 0.299 + green * 0.587 + blue * 0.114);
                    Color greyColor = new Color(gray, gray, gray);
                    newImage.setRGB(x, y, greyColor.getRGB());
                }
            }
            File output = new File(uploadPath + "/" + UUID.randomUUID() + ".png");
            ImageIO.write(newImage, "png", output);

            oldImage.setFileName(output.getName());
            imageRepository.save(oldImage);

            File deleteFile = new File(uploadPath + "/" + oldFileName);
            deleteFile.delete();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void imageRotateR(int id, int rad) {
        Image oldImage = imageRepository.findById(id).orElse(null);
        String oldFileName = oldImage.getFileName();
        try {
            File file = new File(uploadPath + "/" + oldImage.getFileName());
            BufferedImage sourceImage = ImageIO.read(file);
            int width = sourceImage.getWidth();
            int height = sourceImage.getHeight();
            int type = sourceImage.getColorModel().getTransparency();
            BufferedImage img;
            Graphics2D graphics2D;
            (graphics2D = (img = new BufferedImage(width, height, type))
                    .createGraphics()).setRenderingHint(
                    RenderingHints.KEY_INTERPOLATION,
                    RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            graphics2D.rotate(Math.toRadians(rad), width / 2, height / 2);
            graphics2D.drawImage(sourceImage, 0, 0, null);
            graphics2D.dispose();

            File output = new File(uploadPath + "/" + UUID.randomUUID() + ".png");
            ImageIO.write(img, "png", output);

            oldImage.setFileName(output.getName());
            imageRepository.save(oldImage);

            File deleteFile = new File(uploadPath + "/" + oldFileName);
            deleteFile.delete();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void imageResize(int id_image, int size) {
        Image oldImage = imageRepository.findById(id_image).orElse(null);
        String oldFileName = oldImage.getFileName();
        try {
            File file = new File(uploadPath + "/" + oldImage.getFileName());
            BufferedImage sourceImage = ImageIO.read(file);

            int width = sourceImage.getWidth() + size;
            int height = sourceImage.getHeight() + size;
            int type = sourceImage.getColorModel().getTransparency();
            BufferedImage img;
            Graphics2D graphics2D;
            (graphics2D = (img = new BufferedImage(width, height, type))
                    .createGraphics()).setRenderingHint(
                    RenderingHints.KEY_INTERPOLATION,
                    RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            graphics2D.drawImage(sourceImage, 0, 0, width, height, 0, 0, sourceImage
                    .getWidth(), sourceImage.getHeight(), null);
            graphics2D.dispose();

            File output = new File(uploadPath + "/" + UUID.randomUUID() + ".png");
            ImageIO.write(img, "png", output);

            oldImage.setFileName(output.getName());
            imageRepository.save(oldImage);

            File deleteFile = new File(uploadPath + "/" + oldFileName);
            deleteFile.delete();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
