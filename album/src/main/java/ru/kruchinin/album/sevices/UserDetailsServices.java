package ru.kruchinin.album.sevices;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.kruchinin.album.models.User;
import ru.kruchinin.album.repositories.UserRepository;
import ru.kruchinin.album.security.UserDetails;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserDetailsServices implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public org.springframework.security.core.userdetails.UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> User = userRepository.findByUserName(username);
        if (User.isEmpty())
            throw new UsernameNotFoundException("User not found");
        return new UserDetails(User.get());
    }

}
