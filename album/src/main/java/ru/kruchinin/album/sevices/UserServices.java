package ru.kruchinin.album.sevices;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.kruchinin.album.models.Roles;
import ru.kruchinin.album.models.User;
import ru.kruchinin.album.repositories.UserRepository;
import ru.kruchinin.album.security.UserDetails;

import java.util.List;

/**
 * @author Kruchinin Artem 18.08.2022
 */
@Service
@RequiredArgsConstructor
public class UserServices {
    private final UserRepository userRepository;
    private final AlbumServices albumServices;
    private final ImageServices imageServices;
    private final AuthenticationServices authenticationServices;
    @Value("${upload.path}")
    private String uploadPath;


    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public User getUserByName(String name) {
        return userRepository.findByUserName(name).orElse(null);
    }

    public void updateUserEmailWaitActivation(String updatedEmail, int id) {
        User userOld = userRepository.findById(id).orElse(null);
        User user = new User();
        user.setUserEmail(updatedEmail);
        user.setUserName("test123");
        user.setUserPassword("test123");
        user.setCheckRules(true);
        user.setActivationCode(String.valueOf(id));
        userRepository.save(user);
        String message = String.format("Hello %s!\nYou have sent a request to change your Email.\nPlease follow the link for further steps:\nhttp://localhost:8080/user/update-email/%s", userOld.getUserName(), user.getActivationCode());
        authenticationServices.sendEmail(userOld.getUserEmail(), "Change Email", message);
    }

    public void updateUserEmail(String id) {
        User userOld = userRepository.findById(Integer.valueOf(id)).orElse(null);
        User userForWait = userRepository.findByActivationCode(id).orElse(null);
        userOld.setUserEmail(userForWait.getUserEmail());
        userRepository.save(userOld);
        userRepository.delete(userForWait);
    }

    public void delete(int id) {

//        ???


        userRepository.deleteById(id);
    }

    public void banUser(int id) {
        User user = userRepository.findById(id).orElse(null);
        if (user != null) {
            if (user.isActive()) {
                user.setActive(false);
            } else {
                user.setActive(true);
            }
        }
        userRepository.save(user);
    }

    public void changeRole(int id, Roles role) {
        User user = userRepository.findById(id).orElse(null);
        if (user != null) {
            user.setRole(role.name());
            userRepository.save(user);
        }
    }


    // метод возвращает атентефицированного пользователя
    public User authUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return userDetails.getUser();
    }
}
